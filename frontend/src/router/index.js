
import {createWebHistory, createRouter} from 'vue-router';
import TaskList from '../components/TaskList.vue';
import Dashboard from '../components/Dashboard.vue';
import HomeComponent from '../components/HomeComponent.vue';

const routes = [
  { path: '/', component: HomeComponent }, 
  { path: '/dashboard', component: Dashboard, meta: { requiresAuth: true } },
  { path: '/tasks', component: TaskList, meta: { requiresAuth: true } },
];

const router= createRouter({
    history: createWebHistory(),
    routes
});

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  const isAuthenticated = !!localStorage.getItem('token');
  if (requiresAuth && !isAuthenticated) {
    next('/');
  } else {
    next();
  }
});

export default router;
