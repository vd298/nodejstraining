import { createStore } from 'vuex';
import axios from 'axios';
import router from '../router/index'

export default createStore({
  state: {
    token: localStorage.getItem('token') || '',
    user: {},
    users: [],
    tasks: []
  },
  mutations: {
    auth_success(state, token) {
      state.token = token;
    },
    logout(state) {
      state.token = '';
    },
    set_tasks(state, tasks) {
      state.tasks = tasks;
    },
    set_users(state, users) {
      state.users = users;
    },
  },
  actions: {
    async login({ commit }, user) {
      const response = await axios.post('http://localhost:3000/api/auth/login', user);
      const token = response.data.token;
      localStorage.setItem('token', token);
      axios.defaults.headers.common['Authorization'] = token;
      commit('auth_success', token);
    },
    async logout({ commit }) {
      localStorage.removeItem('token');
      delete axios.defaults.headers.common['Authorization'];
      commit('logout');
      router.push('/');
    },
    async fetchTasks({ commit }) {
      const config = {
        headers: {
          Authorization: this.state.token
        }
      };
      const response = await axios.get('http://localhost:3000/api/tasks', config);
      commit('set_tasks', response.data);
    },
    async fetchUsers({ commit }) {
      const config = {
        headers: {
          Authorization: this.state.token
        }
      };
      const response = await axios.get('http://localhost:3000/api/users', config);
      console.log(response);
      commit('set_users', response.data);
    }
  },
  getters: {
    isLoggedIn: state => !!state.token,
    tasks: state => state.tasks,
    users: state => state.users,
    token: state => state.token,
  }
});
