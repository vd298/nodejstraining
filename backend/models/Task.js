"use strict";
const common_fields = require("./CommonFields");

module.exports = (sequelize, DataTypes) => {
  const Task = sequelize.define(
    "Task",
    {
      //...common_fields,

      title: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      summary: {
        type: DataTypes.TEXT,
      },
      assignee: {
        type: DataTypes.STRING(100),
      },
      due_date: {
        type: DataTypes.DATE,
      },
      status: {
        type: DataTypes.STRING(20),
      },
      user_id: {
        type: DataTypes.INTEGER,
        references: {
          model: 'users',
          key: 'id',
        },
      },
    },
    {
      tableName: "tasks",
      timestamps: false,
    }
  );

  return Task;
};
