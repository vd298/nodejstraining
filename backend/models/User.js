"use strict";
const common_fields = require("./CommonFields");

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    "User",
    {
      //...common_fields,

      username: {
        type: DataTypes.STRING(100),
        allowNull: false,
        unique: true,
      },
      password: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING(100),
        allowNull: false,
        unique: true,
      },
    },
    {
      tableName: "users",
      timestamps: false,
    }
  );

  return User;
};
