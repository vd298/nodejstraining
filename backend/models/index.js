const { Sequelize, DataTypes } = require('sequelize');
const sequelize = new Sequelize('vueproj', 'hse', 'hse', {
  host: 'localhost',
  dialect: 'postgres'
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.User = require('./User')(sequelize, DataTypes);
db.Task = require('./Task')(sequelize, DataTypes);

module.exports = db;
