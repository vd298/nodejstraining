const { Pool } = require('pg');

console.log('Connecting to database with connection string:', process.env.DB_CONN_STRING);

const pool = new Pool({
    user: 'hse',
  host: 'localhost',
  database: 'vueproj',
  password: 'hse',
  port: 5432,
});

pool.connect((err, client, release) => {
  if (err) {
    console.error('Error connecting to the database:', err);
    return;
  }
  console.log('Successfully connected to the database');
  release();
});

module.exports = pool;
