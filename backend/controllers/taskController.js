const db = require('../models');
const Task = db.Task;

exports.createTask = async (req, res) => {
  const { title, summary, assignee, due_date, status } = req.body;
  const userId = req.user.userId;

  try {
    const task = await Task.create({
      title,
      summary,
      assignee,
      due_date,
      status,
      user_id: userId
    });
    res.status(201).json(task);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

exports.getTasks = async (req, res) => {
  const userId = req.user.userId;

  try {
    const tasks = await Task.findAll({ where: { user_id: userId } });
    res.status(200).json(tasks);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

exports.updateTask = async (req, res) => {
  const { title, summary, assignee, due_date, status } = req.body;
  const { taskId } = req.params;
  const userId = req.user.userId;

  try {
    const [updated] = await Task.update({
      title,
      summary,
      assignee,
      due_date,
      status
    }, {
      where: { id: taskId, user_id: userId }
    });

    if (updated) {
      const updatedTask = await Task.findOne({ where: { id: taskId } });
      res.status(200).json(updatedTask);
    } else {
      res.status(404).json({ error: 'Task not found' });
    }
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

exports.deleteTask = async (req, res) => {
  const { taskId } = req.params;
  const userId = req.user.userId;

  try {
    const deleted = await Task.destroy({ where: { id: taskId, user_id: userId } });

    if (deleted) {
      res.status(200).json({ message: 'Task deleted' });
    } else {
      res.status(404).json({ error: 'Task not found' });
    }
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};
