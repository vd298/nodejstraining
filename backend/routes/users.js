const express = require('express');
const router = express.Router();
const authMiddleware = require('../middleware/authMiddleware');
const { getUsers, getUserById } = require('../controllers/userController');

router.use(authMiddleware);

router.get('/', getUsers);

router.get('/:userId', getUserById);

module.exports = router;
